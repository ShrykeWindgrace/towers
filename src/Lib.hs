module Lib where

import Data.List
import Lens.Micro
import Control.Applicative

someFunc :: IO ()
someFunc = putStrLn "someFunc"

type N = Int

data Cell = Known N | Hypothesis [N] deriving (Eq,Show)

type Sol = [[Cell]]

data Clues = Clues {
    north :: [Maybe N],
    east :: [Maybe N],
    south :: [Maybe N],
    west :: [Maybe N]
} deriving (Eq,Show)

data Direction = North | East | West | South deriving (Eq,Show)

type Rule = Direction -> N -> Sol -> Sol

type Recon = Direction -> N -> Sol -> Sol

remKnown :: N -> Cell -> Cell
remKnown n (Hypothesis ns) = case n `delete` ns of
    [] -> error "logic error"
    [x] -> Known x
    xs -> Hypothesis xs
remKnown _ y = y

remKnownRow :: N -> N -> Sol -> Sol
remKnownRow what i = over (ix i) (fmap (remKnown what))

remKnownColumn :: N -> N -> Sol -> Sol
remKnownColumn what j target =  let size = length target in foldl1 (.) [ over (ix i . ix j) (remKnown what)| i <- [0..size]] target

initial :: N -> Sol
initial n = replicate n $ replicate n (Hypothesis [1..n])

put :: N -> N -> N -> Sol -> Sol
put what i j = set (ix i  . ix j) (Known what)

putAndRemove :: N -> N -> N -> Sol -> Sol
putAndRemove what i j target = target & put what i j & remKnownColumn what j & remKnownRow what i

validate :: N -> [[N]] -> Bool
validate size = liftA2 (&&) (validateRows size) (validateColumns size)

validateRows,  validateColumns :: N -> [[N]] -> Bool
validateRows size = all (\ns -> sort ns == [1..size])
validateColumns size = validateRows size . transpose

