import Test.Hspec
import Lib
main :: IO ()
main = hspec $ do
    describe "removing numbers from a cell" $ do
        it "identity" $
            remKnown 1 (Hypothesis [2,3]) `shouldBe` Hypothesis [2,3]
        it "one number remaining" $
            remKnown 1 (Hypothesis [1,2]) `shouldBe` Known 2
        it "one number remaining" $
            remKnown 1 (Hypothesis [1,2,3]) `shouldBe` Hypothesis [2,3]

    describe "removing numbers in a row" $
        it "2x2" $
            remKnownRow 1 0 [[Known 1, Hypothesis [1,2]], [Hypothesis [1,2], Hypothesis [1,2]]] `shouldBe` [[Known 1, Known 2], [Hypothesis [1,2], Hypothesis [1,2]]]
    describe "removing numbers in a column" $
        it "2x2" $
            remKnownColumn 1 0 [[Known 1, Hypothesis [1,2]], [Hypothesis [1,2], Hypothesis [1,2]]] `shouldBe` [[Known 1, Hypothesis [1,2]], [Known 2 , Hypothesis [1,2]]]
    describe "put number" $
        it "2x2" $
            put 1 0 0 (initial 2) `shouldBe` [[Known 1, Hypothesis [1,2]], [Hypothesis [1,2] , Hypothesis [1,2]]]
    describe "put and remove number" $
        it "2x2" $
            putAndRemove 1 0 0 (initial 2) `shouldBe` [[Known 1, Known 2], [Known 2 , Hypothesis [1,2]]]
    describe "validation" $ do
        it "naive, 2x2" $ do
            validate 2 [[1,2],[2,1]] `shouldBe` True
            validate 2 [[1,2],[1,2]] `shouldBe` False

